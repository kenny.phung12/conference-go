[33mcommit 3226f61cfd3d2b4d2c740177f7ae76217e7d3b17[m[33m ([m[1;36mHEAD -> [m[1;32mmain[m[33m, [m[1;31morigin/main[m[33m, [m[1;31morigin/HEAD[m[33m)[m
Author: Kenny Phung <kenny.phung12@gmail.com>
Date:   Tue May 16 00:31:05 2023 -0700

    D5 Dockerized

[33mcommit 4d426eba0da68c80dff80d2f34086f0a028276b0[m
Author: Kenny Phung <kenny.phung12@gmail.com>
Date:   Mon May 15 23:51:45 2023 -0700

    D3 'RESTfulize'; all views successfully converted. D4 'Integrating 3rd Party Data' Semi-Completed; photo_url=null

[33mcommit c7b1a2f525531110620169fcb61980b1c8d3c7d8[m
Author: Kenny Phung <kenny.phung12@gmail.com>
Date:   Tue May 9 22:57:24 2023 -0700

    D2 DIY JSON Library Completed

[33mcommit 8488b458b0ff4c3d5423f85ffb0916b3c0a40416[m
Author: Kenny Phung <kenny.phung12@gmail.com>
Date:   Tue May 9 20:12:25 2023 -0700

    D1 Json Views Completed

[33mcommit eb9112e74e7581ffc2eae45c46114b1c5d84b4a6[m
Author: Bart Dorsey <bart@bartdorsey.com>
Date:   Sat Nov 5 11:53:12 2022 -0500

    Changing all occurances of pk to id to match module 1 content.

[33mcommit 02611920290e8d0a30ed0f3e3d6f22df80a3c9ec[m
Author: Hack Reactor <>
Date:   Tue Feb 1 09:28:52 2022 -0500

    Initial import.
