from .keys import PEXELS_API_KEY, OPEN_WEATHER_API_KEY
import requests
import json


def get_photo(city, state):
    # Create a dictionary for the headers to use in the request
    headers = {"Authorization", PEXELS_API_KEY}

    # Create the URL for the request with the city and state
    url = "https://api.pexels.com/v1/search"
    query = f"{city}, {state}"
    payload = {"query": query, "per_page": 1}

    # Make the request
    response = requests.get(url, headers=headers, params=payload)

    # Parse the JSON response
    content = json.loads(response.content)
    photo_url = content['photos']['url']
    # Return a dictionary that contains a `picture_url` key and
    #   one of the URLs for one of the pictures in the response
    return {"picture_url": photo_url}


def get_weather_data(city, state):
    # Create the URL for the geocoding API with the city and state
    payload = {"q": f"{city}, {state}, us", "appid": OPEN_WEATHER_API_KEY}
    url = "https://api.openweathermap.org/geo/1.0/direct"

    # Make the request
    response = requests.get(url, params=payload)

    # Parse the JSON response
    content = json.loads(response.content)

    # Get the latitude and longitude from the response
    latitude = content[0]['lat']
    longitude = content[0]['lon']

    # Create the URL for the current weather API with the latitude
    #   and longitude
    weather_payload = {"lat": latitude, "lon": longitude, "appid": OPEN_WEATHER_API_KEY}
    weather_url = "https://api.openweathermap.org/data/2.5/weather"
    # Make the request
    weather_response = requests.get(weather_url, params=weather_payload)
    weather_content = json.loads(weather_response.content)
    if not weather_content:
        return None

    # Parse the JSON response
    # Get the main temperature and the weather's description and put
    #   them in a dictionary
    main_temperature = weather_content["main"]["temp"]
    weather_description = weather_content["weather"][0]["description"]
    weather_data = {"temperature": main_temperature, "description": weather_description}
    # Return the dictionary
    return weather_data
